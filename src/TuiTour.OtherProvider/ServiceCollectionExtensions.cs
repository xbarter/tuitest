﻿namespace TuiTour.OtherProvider
{
    using Microsoft.Extensions.DependencyInjection;
    using TuiTour.Common.Services;

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddOtherProvider(this IServiceCollection services)
        {
            services.AddSingleton<ISearchService, SearchService>();
            return services;
        }
    }
}
