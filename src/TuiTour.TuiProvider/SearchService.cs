﻿namespace TuiTour.TuiProvider
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using Common;
    using Common.Settings;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using TuiTour.Common.Models;
    using TuiTour.Common.Services;

    sealed class SearchService : ISearchService
    {
        readonly IQueryable<Tour> _tours = new AsyncEnumerableQuery<Tour>(new Tour[0]);

        public SearchService(IOptions<AppSettings> settings)
        {
            Settings = settings;
        }

        IOptions<AppSettings> Settings { get; }

        /// <inheritdoc />
        public IAsyncEnumerable<Tour> GetByDepartureCity(City departureCity, SortingType sorting = SortingType.None)
        {
            var query = _tours.Where(tour => tour.DepartureCity.Name.ToUpperInvariant() == departureCity.Name.ToUpperInvariant() &&
                                             tour.DepartureCity.Country.Name.ToUpperInvariant() == departureCity.Country.Name.ToUpperInvariant());

            query = Sort(query, sorting);

            return EnumerateAsync(query, Settings.Value.OperationTimeout);
        }

        /// <inheritdoc />
        public IAsyncEnumerable<Tour> GetByCity(City city, SortingType sorting = SortingType.None)
        {
            var query = _tours.Where(tour => tour.Hotel.City.Name.ToUpperInvariant() == city.Name.ToUpperInvariant() &&
                                             tour.Hotel.City.Country.Name.ToUpperInvariant() == city.Country.Name.ToUpperInvariant());

            query = Sort(query, sorting);

            return EnumerateAsync(query, Settings.Value.OperationTimeout);
        }

        /// <inheritdoc />
        public IAsyncEnumerable<Tour> GetByDate(DateTimeOffset date, SortingType sorting = SortingType.None)
        {
            var query = _tours.Where(tour => tour.ArrivalDate == date);

            query = Sort(query, sorting);

            return EnumerateAsync(query, Settings.Value.OperationTimeout);
        }

        /// <inheritdoc />
        public IAsyncEnumerable<Tour> GetByNights(int @from, int to, SortingType sorting = SortingType.None)
        {
            var query = _tours.Where(tour => tour.Nights > @from && tour.Nights < to);

            query = Sort(query, sorting);

            return EnumerateAsync(query, Settings.Value.OperationTimeout);
        }

        /// <inheritdoc />
        public IAsyncEnumerable<Tour> GetByPersons(int persons, SortingType sorting = SortingType.None)
        {
            var query = _tours.Where(tour => tour.MaxPerson == persons);

            query = Sort(query, sorting);

            return EnumerateAsync(query, Settings.Value.OperationTimeout);
        }

        /// <inheritdoc />
        public IAsyncEnumerable<Tour> GetTours(SortingType sorting = SortingType.None)
        {
            var query = Sort(_tours, sorting);

            return EnumerateAsync(query, Settings.Value.OperationTimeout);
        }

        static IQueryable<Tour> Sort(IQueryable<Tour> query, SortingType sorting)
        {
            switch (sorting)
            {
                case SortingType.ByPrice:
                    return query.OrderBy(tour => tour.PricePerPerson);
                case SortingType.ByPriceDesc:
                    return query.OrderByDescending(tour => tour.PricePerPerson);
                case SortingType.ByName:
                    return query.OrderBy(tour => tour.Hotel.Name);
                case SortingType.ByDate:
                    return query.OrderBy(tour => tour.ArrivalDate);
                case SortingType.ByDateDesc:
                    return query.OrderByDescending(tour => tour.ArrivalDate);
            }

            return query;
        }

        static async IAsyncEnumerable<Tour> EnumerateAsync(IQueryable<Tour> query, TimeSpan timeout)
        {
            await using var enumerator = query.AsAsyncEnumerable().GetAsyncEnumerator();

            using var cancellation = new CancellationTokenSource(timeout);

            while (await enumerator.MoveNextAsync())
            {
                if (cancellation.IsCancellationRequested)
                {
                    throw new TimeoutException();
                }

                yield return enumerator.Current;
            }
        }
    }
}
