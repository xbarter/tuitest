﻿namespace TuiTour.TuiProvider
{
    using Microsoft.Extensions.DependencyInjection;
    using TuiTour.Common.Services;

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddTuiProvider(this IServiceCollection services)
        {
            services.AddSingleton<ISearchService, SearchService>();
            return services;
        }
    }
}
