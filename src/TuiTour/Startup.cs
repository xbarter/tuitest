namespace TuiTour
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Common.Services;
    using Common.Settings;
    using Filters;
    using FluentValidation.AspNetCore;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Diagnostics;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.OpenApi.Models;
    using Services;
    using TuiTour.OtherProvider;
    using TuiTour.TuiProvider;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            ConfigureTourProviders(services);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" });
            });

            services.Configure<AppSettings>(Configuration.GetSection(nameof(AppSettings)));

            services.AddMvc(opt => opt.Filters.Add(typeof(ValidatorActionFilter)))
                    .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                    .AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(errorApp => errorApp.Run(HandlerError));
            }

            app.UseSwagger();

            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1"));

            app.UseHttpsRedirection()
                .UseAuthorization()
                .UseRouting()
                .UseEndpoints(endpoints => endpoints.MapControllers());
        }

        static IServiceCollection ConfigureTourProviders(IServiceCollection services)
        {
            services.AddTuiProvider()
                    .AddOtherProvider();

            services.AddSingleton<ITourAggregator, TourAggregator>();
            services.AddSingleton<ISearchAggregatorService, SearchAggregatorService>();

            return services;
        }

        static async Task HandlerError(HttpContext context)
        {
            context.Response.StatusCode = 500;
            context.Response.ContentType = "text/html";

            await context.Response.WriteAsync("<html lang=\"en\"><body>\r\n");
            await context.Response.WriteAsync("ERROR!<br><br>\r\n");

            var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();

            var exception = exceptionHandlerPathFeature?.Error;

            switch (exception)
            {
                case FileNotFoundException _:
                    await context.Response.WriteAsync("File error thrown!<br><br>\r\n");
                    break;
                case NotImplementedException _:
                    await context.Response.WriteAsync("Not implemented error thrown!<br><br>\r\n");
                    break;
                case TimeoutException _:
                    await context.Response.WriteAsync("Timeout error thrown!<br><br>\r\n");
                    break;
                default:
                    await context.Response.WriteAsync("Unknown error thrown!<br><br>\r\n");
                    break;
            }

            await context.Response.WriteAsync("<a href=\"/\">Home</a><br>\r\n");
            await context.Response.WriteAsync("</body></html>\r\n");
            await context.Response.WriteAsync(new string(' ', 512));
        }
    }
}
