﻿namespace TuiTour.Validators
{
    using Common.Models;
    using FluentValidation;

    public class CountryValidator : AbstractValidator<Country>
    {
        public CountryValidator()
        {
            RuleFor(c => c.Name).NotEmpty();
        }
    }
}