﻿namespace TuiTour.Validators
{
    using System;
    using Common.Models;
    using FluentValidation;

    public class HotelValidator : AbstractValidator<Hotel>
    {
        public HotelValidator()
        {
            RuleFor(c => c.Name).NotEmpty();
            RuleFor(c => c.City).SetValidator(new CityValidator());
            RuleFor(c => c.Address).NotEmpty();
            RuleFor(c => c.BuildTime).GreaterThan(DateTimeOffset.MinValue);
        }
    }
}