﻿namespace TuiTour.Validators
{
    using Common.Models;
    using FluentValidation;

    public class CityValidator : AbstractValidator<City>
    {
        public CityValidator()
        {
            RuleFor(c => c.Name).NotEmpty();
            RuleFor(c => c.Country).SetValidator(new CountryValidator());
        }
    }
}
