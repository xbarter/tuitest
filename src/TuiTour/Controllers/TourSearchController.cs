﻿namespace TuiTour.Controllers
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Mvc;
    using Services;
    using TuiTour.Common.Models;
    using TuiTour.Common.Services;

    [ApiController]
    [Route("api/[controller]")]
    public sealed class TourSearchController : ControllerBase
    {
        public TourSearchController(ISearchAggregatorService searchProvider)
        {
            SearchProvider = searchProvider;
        }

        ISearchAggregatorService SearchProvider { get; }

        [HttpPost]
        public async IAsyncEnumerable<Tour> GetToursByCity(City city, SortingType sorting = SortingType.None)
        {
            var tours = SearchProvider.GetByCity(city, sorting);

            await foreach (var tour in tours)
            {
                yield return tour;
            }
        }
    }
}
