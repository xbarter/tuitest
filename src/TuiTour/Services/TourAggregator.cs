﻿namespace TuiTour.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Models;
    using Common.Settings;
    using Microsoft.Extensions.Options;

    class TourAggregator : ITourAggregator
    {
        public TourAggregator(IOptions<AppSettings> settings)
        {
            Settings = settings;
        }

        IOptions<AppSettings> Settings { get; }

        public async IAsyncEnumerable<Tour> Aggregate(IAsyncEnumerable<Tour> source)
        {
            var groupTours = source.GroupBy(tour =>
            (
                tour.Hotel.Name,
                tour.Hotel.City.Name,
                tour.Hotel.City.Country.Name,
                tour.Hotel.City.Country.Name,
                tour.Hotel.Address,
                tour.DepartureCity.Name,
                tour.DepartureCity.Country.Name,
                tour.ArrivalDate,
                tour.CheckInDate,
                tour.DepartureDate,
                tour.Nights,
                tour.RoomType
            ));

            await foreach (var group in groupTours)
            {
                yield return await group.AggregateAsync(Accumulator);
            }
        }

        internal Tour Accumulator(Tour tour, Tour nextTour)
        {
            if (tour.Provider == Settings.Value.PreferredProvider)
            {
                return NextTour(tour, nextTour);
            }

            if (nextTour.Provider == Settings.Value.PreferredProvider)
            {
                return NextTour(nextTour, tour);
            }

            return tour.PricePerPerson - nextTour.PricePerPerson < 0 ? tour : nextTour;

            Tour NextTour(Tour t1, Tour t2)
            {
                var excess = t1.PricePerPerson * Settings.Value.MaxPriceExcess;
                var diff = t1.PricePerPerson - t2.PricePerPerson;
                return diff > excess ? t2 : t1;
            }
        }
    }
}
