﻿namespace TuiTour.Services
{
    using System.Collections.Generic;
    using Common.Models;

    public interface ITourAggregator
    {
        IAsyncEnumerable<Tour> Aggregate(IAsyncEnumerable<Tour> source);
    }
}