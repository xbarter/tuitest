﻿namespace TuiTour.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using TuiTour.Common.Models;
    using TuiTour.Common.Services;

    public class SearchAggregatorService : ISearchAggregatorService
    {
        public SearchAggregatorService(IEnumerable<ISearchService> searchProviders, ITourAggregator aggregator)
        {
            SearchProviders = searchProviders;
            Aggregator = aggregator;
        }

        IEnumerable<ISearchService> SearchProviders { get; }
        ITourAggregator Aggregator { get; }

        public async IAsyncEnumerable<Tour> GetByDepartureCity(City departureCity, SortingType sorting = SortingType.None)
        {
            var tours = SearchProviders.Select(p => p.GetByDepartureCity(departureCity, sorting)).OnErrorResumeNext();

            await foreach (var tour in Aggregator.Aggregate(tours))
            {
                yield return tour;
            }
        }

        public async IAsyncEnumerable<Tour> GetByCity(City city, SortingType sorting = SortingType.None)
        {
            var tours = SearchProviders.Select(p => p.GetByCity(city, sorting)).OnErrorResumeNext();

            await foreach (var tour in Aggregator.Aggregate(tours))
            {
                yield return tour;
            }
        }

        public async IAsyncEnumerable<Tour> GetByDate(DateTimeOffset date, SortingType sorting = SortingType.None)
        {
            var tours = SearchProviders.Select(p => p.GetByDate(date, sorting)).OnErrorResumeNext();

            await foreach (var tour in Aggregator.Aggregate(tours))
            {
                yield return tour;
            }
        }

        public async IAsyncEnumerable<Tour> GetByNights(int @from, int to, SortingType sorting = SortingType.None)
        {
            var tours = SearchProviders.Select(p => p.GetByNights(@from, to, sorting)).OnErrorResumeNext();

            await foreach (var tour in Aggregator.Aggregate(tours))
            {
                yield return tour;
            }
        }

        public async IAsyncEnumerable<Tour> GetByPersons(int persons, SortingType sorting = SortingType.None)
        {
            var tours = SearchProviders.Select(p => p.GetByPersons(persons, sorting)).OnErrorResumeNext();

            await foreach (var tour in Aggregator.Aggregate(tours))
            {
                yield return tour;
            }
        }

        public async IAsyncEnumerable<Tour> GetTours(SortingType sorting = SortingType.None)
        {
            var tours = SearchProviders.Select(p => p.GetTours(sorting)).OnErrorResumeNext();

            await foreach (var tour in Aggregator.Aggregate(tours))
            {
                yield return tour;
            }
        }
    }
}