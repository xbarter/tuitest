﻿namespace TuiTour.Common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class AsyncEnumerator<T> : IAsyncEnumerator<T>
	{
        readonly IEnumerator<T> _inner;

		public AsyncEnumerator(IEnumerator<T> inner)
		{
			_inner = inner;
		}

        public ValueTask<bool> MoveNextAsync()
        {
            return new ValueTask<bool>(Task.FromResult(_inner.MoveNext()));
        }

        public T Current => _inner.Current;

        public ValueTask DisposeAsync()
        {
            _inner.Dispose();

            return new ValueTask(Task.CompletedTask);
        }
    }
}