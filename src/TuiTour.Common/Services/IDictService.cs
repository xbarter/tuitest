﻿namespace TuiTour.Common.Services
{
    using System.Collections.Generic;
    using TuiTour.Common.Models;

    /// <summary>
    ///     Dictionary service.
    /// </summary>
    public interface IDictService
    {
        /// <summary>
        ///     Gets departure cities.
        /// </summary>
        IAsyncEnumerable<City> GetDepartureCities();

        /// <summary>
        ///     Gets cities.
        /// </summary>
        /// <returns></returns>
        IAsyncEnumerable<City> GetCities();

        /// <summary>
        ///     Gets countries.
        /// </summary>
        IAsyncEnumerable<City> GetCountries();

        /// <summary>
        ///     Gets hotels by id. TODO
        /// </summary>
        /// <param name="id">Hotel id.</param>
        IAsyncEnumerable<Hotel> GetHotels(long id);
    }
}