﻿namespace TuiTour.Common.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using TuiTour.Common.Models;

    /// <summary>
    ///     Search service.
    /// </summary>
    public interface ISearchService
    {
        /// <summary>
        ///     Search tours by departure city.
        /// </summary>
        /// <param name="departureCity">Departure city.</param>
        /// <param name="sorting">Sorting type.</param>
        IAsyncEnumerable<Tour> GetByDepartureCity(City departureCity, SortingType sorting = SortingType.None);

        /// <summary>
        ///     Search tours by city.
        /// </summary>
        /// <param name="city">Tour city.</param>
        /// <param name="sorting">Sorting type.</param>
        IAsyncEnumerable<Tour> GetByCity(City city, SortingType sorting = SortingType.None);

        /// <summary>
        ///     Search tours by date.
        /// </summary>
        /// <param name="date">Tour date.</param>
        /// <param name="sorting">Sorting type.</param>
        IAsyncEnumerable<Tour> GetByDate(DateTimeOffset date, SortingType sorting = SortingType.None);

        /// <summary>
        ///     Search tours by nights.
        /// </summary>
        /// <param name="from">From nights.</param>
        /// <param name="to">To nights.</param>
        /// <param name="sorting">Sorting type.</param>
        IAsyncEnumerable<Tour> GetByNights(int @from, int to, SortingType sorting = SortingType.None);

        /// <summary>
        ///     Search tours by persons.
        /// </summary>
        /// <param name="persons"></param>
        /// <param name="sorting">Sorting type.</param>
        /// <returns></returns>
        IAsyncEnumerable<Tour> GetByPersons(int persons, SortingType sorting = SortingType.None);

        /// <summary>
        ///     Get tours.
        /// </summary>
        /// <param name="sorting">Sorting type.</param>
        IAsyncEnumerable<Tour> GetTours(SortingType sorting = SortingType.None);
    }
}