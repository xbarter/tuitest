﻿namespace TuiTour.Common.Services
{
    public enum SortingType
    {
        None = 0,
        ByPrice = 1,
        ByPriceDesc = 2,
        ByName = 3,
        ByDate = 4,
        ByDateDesc = 5,
    }
}