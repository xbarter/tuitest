﻿namespace TuiTour.Common
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading;

    /// <inheritdoc cref="ICriticalNotifyCompletion" />
    /// <summary>
    ///     Switches to tread pool if not already there.
    /// </summary>
    /// <example><c>await default(SwitchToThreadPool);</c></example>
    public readonly struct SwitchToThreadPool : ICriticalNotifyCompletion
    {
        static readonly WaitCallback Callback = state => (state as Action)?.Invoke();

        readonly bool _alwaysYield;

        public SwitchToThreadPool(bool alwaysYield)
        {
            _alwaysYield = alwaysYield;
        }

        public bool IsCompleted => !_alwaysYield && Thread.CurrentThread.IsThreadPoolThread;

        public void OnCompleted(Action continuation)
        {
            ThreadPool.QueueUserWorkItem(Callback, continuation);
        }
        public void UnsafeOnCompleted(Action continuation)
        {
            ThreadPool.UnsafeQueueUserWorkItem(Callback, continuation);
        }

        public SwitchToThreadPool GetAwaiter() => this;

        public void GetResult()
        {
        }
    }
}