﻿namespace TuiTour.Common.Models
{
    using System;

    /// <summary>
    ///     Hotel model.
    /// </summary>
    public class Hotel
    {
        /// <summary>
        ///     Hotel Id.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        ///     Hotel name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        ///     City where hotel placed.
        /// </summary>
        public City City { get; set; }

        /// <summary>
        ///     Build time.
        /// </summary>
        public DateTimeOffset BuildTime { get; set; }
    }
}
