﻿namespace TuiTour.Common.Models
{
    public enum RoomType
    {
        Standard = 0,
        Deluxe = 1,
        Lux = 2,
    }
}