﻿namespace TuiTour.Common.Models
{
    /// <summary>
    ///     Country model.
    /// </summary>
    public class Country
    {
        /// <summary>
        ///     Country Id.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        ///     Country name.
        /// </summary>
        public string Name { get; set; }
    }
}
