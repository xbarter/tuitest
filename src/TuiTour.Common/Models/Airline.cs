﻿namespace TuiTour.Common.Models
{
    /// <summary>
    ///     Airline model.
    /// </summary>
    public class Airline
    {
        /// <summary>
        ///     Airline Id.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        ///     Airline name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Airline short name.
        /// </summary>
        public string ShortName { get; set; }
    }
}
