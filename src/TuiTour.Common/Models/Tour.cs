﻿namespace TuiTour.Common.Models
{
    using System;

    /// <summary>
    ///     Tour model.
    /// </summary>
    public class Tour
    {
        /// <summary>
        ///     Tour Id.
        /// </summary>
        public long Id { get; set; }

        /* провайдер */
        /// <summary>
        ///     Provider. TODO
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        ///     Residence hotel.
        /// </summary>
        public Hotel Hotel { get; set; }

        /// <summary>
        ///     Room type.
        /// </summary>
        public RoomType RoomType { get; set; }

        /// <summary>
        ///     Departure city.
        /// </summary>
        public City DepartureCity { get; set; }

        /// <summary>
        ///     Departure date.
        /// </summary>
        public DateTimeOffset DepartureDate { get; set; }

        /// <summary>
        ///     Arrival date.
        /// </summary>
        public DateTimeOffset ArrivalDate { get; set; }

        /// <summary>
        ///     Check-in date.
        /// </summary>
        public DateTimeOffset CheckInDate { get; set; }

        /// <summary>
        ///     Night count.
        /// </summary>
        public int Nights { get; set; }

        /// <summary>
        ///     Price per person.
        /// </summary>
        public decimal PricePerPerson { get; set; }

        /// <summary>
        ///     Airline. TODO
        /// </summary>
        public string Airline { get; set; }

        /// <summary>
        ///     Max person count in room.
        /// </summary>
        public int MaxPerson { get; set; }
    }
}