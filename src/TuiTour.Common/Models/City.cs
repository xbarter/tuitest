﻿namespace TuiTour.Common.Models
{
    /// <summary>
    ///  City model.
    /// </summary>
    public class City
    {
        /// <summary>
        ///     City Id.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        ///     City name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Country where the city placed.
        /// </summary>
        public Country Country { get; set; }
    }
}
