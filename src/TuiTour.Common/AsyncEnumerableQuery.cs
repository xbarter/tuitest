﻿namespace TuiTour.Common
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading;

    public class AsyncEnumerableQuery<T> : EnumerableQuery<T>, IAsyncEnumerable<T>, IQueryable<T>
	{
		public AsyncEnumerableQuery(IEnumerable<T> enumerable)
			: base(enumerable)
		{
		}

		public AsyncEnumerableQuery(Expression expression)
			: base(expression)
		{
		}

		IQueryProvider IQueryable.Provider => new AsyncQueryProvider<T>(this);

        IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken cancellationToken)
        {
            return new AsyncEnumerator<T>(this.AsEnumerable().GetEnumerator());
        }
    }
}