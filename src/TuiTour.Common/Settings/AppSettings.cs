﻿namespace TuiTour.Common.Settings
{
    using System;

    public sealed class AppSettings
    {
        /// <summary>
        ///     Preferred provider.
        /// </summary>
        public string PreferredProvider { get; set; }

        /// <summary>
        ///     Max price excess.
        /// </summary>
        public decimal MaxPriceExcess { get; set; }

        /// <summary>
        ///     Operation timeout.
        /// </summary>
        public TimeSpan OperationTimeout { get; set; }
    }
}
