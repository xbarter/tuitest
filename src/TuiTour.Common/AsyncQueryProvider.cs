﻿namespace TuiTour.Common
{
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading;
    using Microsoft.EntityFrameworkCore.Query.Internal;

    public class AsyncQueryProvider<T> : IAsyncQueryProvider
	{
        readonly IQueryProvider _inner;

		public AsyncQueryProvider(IQueryProvider inner)
		{
			_inner = inner;
		}

		public IQueryable CreateQuery(Expression expression)
		{
			return new AsyncEnumerableQuery<T>(expression);
		}

		public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
		{
			return new AsyncEnumerableQuery<TElement>(expression);
		}

		public object Execute(Expression expression)
		{
			return _inner.Execute(expression);
		}

		public TResult Execute<TResult>(Expression expression)
		{
			return _inner.Execute<TResult>(expression);
		}

        public TResult ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
    }
}