namespace TuiTour.Tests
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Common.Models;
    using Common.Settings;
    using Microsoft.Extensions.Options;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Services;

    [TestClass]
    public class TourAggregatorTests
    {
        /*(Hotel, DepartureCity, ArrivalDate, CheckInDate, DepartureDate, Nights, RoomType)*/

        IOptions<AppSettings> _options;
        TourAggregator _tourAggregator;

        [TestInitialize]
        public void SetUp()
        {
            var appSettings = new AppSettings
            {
                MaxPriceExcess = 0.05M,
                PreferredProvider = "Tui",
            };

            _options = Options.Create(appSettings);

            _tourAggregator = new TourAggregator(_options);
        }

        [TestMethod]
        public void AccumulatorTest()
        {
            var departureCountry = new Country { Name = "AAssDD" };
            var arrivalCountry = new Country { Name = "aasSSddFF" };

            var departureCity = new City { Name = "Asdfg", Country = departureCountry };
            var arrivalCity = new City { Name = "FFgFF", Country = arrivalCountry };

            var hotel = new Hotel
            {
                Address = "ddddd",
                BuildTime = DateTimeOffset.UtcNow,
                City = arrivalCity,
                Name = "Grand Butta",
            };

            var tour1 = new Tour
            {
                PricePerPerson = 10,
                ArrivalDate = new DateTimeOffset(2019, 11, 24, 10, 0, 0, TimeSpan.Zero),
                DepartureDate = new DateTimeOffset(2019, 11, 24, 1, 0, 0, TimeSpan.Zero),
                CheckInDate = new DateTimeOffset(2019, 11, 24, 11, 0, 0, TimeSpan.Zero),
                MaxPerson = 3,
                Nights = 10,
                Provider = "Tui",
                RoomType = RoomType.Standard,
                DepartureCity = departureCity,
                Hotel = hotel,
            };

            var tour2 = new Tour
            {
                PricePerPerson = 10,
                ArrivalDate = new DateTimeOffset(2019, 11, 24, 10, 0, 0, TimeSpan.Zero),
                DepartureDate = new DateTimeOffset(2019, 11, 24, 1, 0, 0, TimeSpan.Zero),
                CheckInDate = new DateTimeOffset(2019, 11, 24, 11, 0, 0, TimeSpan.Zero),
                MaxPerson = 3,
                Nights = 10,
                Provider = "sth",
                RoomType = RoomType.Standard,
                DepartureCity = departureCity,
                Hotel = hotel,
            };

            var tour = _tourAggregator.Accumulator(tour1, tour2);
            Assert.AreSame(tour1, tour);

            tour = _tourAggregator.Accumulator(tour2, tour1);
            Assert.AreSame(tour1, tour);

            var priceExcess = tour1.PricePerPerson * _options.Value.MaxPriceExcess;

            var tour3 = new Tour
            {
                PricePerPerson = tour1.PricePerPerson - priceExcess - 0.05M,
                ArrivalDate = new DateTimeOffset(2019, 11, 24, 10, 0, 0, TimeSpan.Zero),
                DepartureDate = new DateTimeOffset(2019, 11, 24, 1, 0, 0, TimeSpan.Zero),
                CheckInDate = new DateTimeOffset(2019, 11, 24, 11, 0, 0, TimeSpan.Zero),
                MaxPerson = 3,
                Nights = 10,
                Provider = "sth",
                RoomType = RoomType.Standard,
                DepartureCity = departureCity,
                Hotel = hotel,
            };

            tour = _tourAggregator.Accumulator(tour1, tour3);
            Assert.AreSame(tour3, tour);

            var tour4 = new Tour
            {
                PricePerPerson = 10,
                ArrivalDate = new DateTimeOffset(2019, 11, 24, 10, 0, 0, TimeSpan.Zero),
                DepartureDate = new DateTimeOffset(2019, 11, 24, 1, 0, 0, TimeSpan.Zero),
                CheckInDate = new DateTimeOffset(2019, 11, 24, 11, 0, 0, TimeSpan.Zero),
                MaxPerson = 3,
                Nights = 10,
                Provider = "sth",
                RoomType = RoomType.Standard,
                DepartureCity = departureCity,
                Hotel = hotel,
            };

            var tour5 = new Tour
            {
                PricePerPerson = 9,
                ArrivalDate = new DateTimeOffset(2019, 11, 24, 10, 0, 0, TimeSpan.Zero),
                DepartureDate = new DateTimeOffset(2019, 11, 24, 1, 0, 0, TimeSpan.Zero),
                CheckInDate = new DateTimeOffset(2019, 11, 24, 11, 0, 0, TimeSpan.Zero),
                MaxPerson = 3,
                Nights = 10,
                Provider = "sth",
                RoomType = RoomType.Standard,
                DepartureCity = departureCity,
                Hotel = hotel,
            };

            tour = _tourAggregator.Accumulator(tour4, tour5);
            Assert.AreSame(tour5, tour);
        }

        [TestMethod]
        public async Task AggregateTest()
        {
            var departureCountry = new Country { Name = "AAssDD" };
            var arrivalCountry = new Country { Name = "aasSSddFF" };

            var departureCity = new City { Name = "Asdfg", Country = departureCountry };
            var arrivalCity = new City { Name = "FFgFF", Country = arrivalCountry };

            var hotel = new Hotel
            {
                Address = "ddddd",
                BuildTime = DateTimeOffset.UtcNow,
                City = arrivalCity,
                Name = "Grand Buthta",
            };

            var tour1 = new Tour
            {
                PricePerPerson = 10,
                ArrivalDate = new DateTimeOffset(2019, 11, 24, 10, 0, 0, TimeSpan.Zero),
                DepartureDate = new DateTimeOffset(2019, 11, 24, 1, 0, 0, TimeSpan.Zero),
                CheckInDate = new DateTimeOffset(2019, 11, 24, 11, 0, 0, TimeSpan.Zero),
                MaxPerson = 3,
                Nights = 10,
                Provider = "Tui",
                RoomType = RoomType.Standard,
                DepartureCity = departureCity,
                Hotel = hotel,
            };

            var tour2 = new Tour
            {
                PricePerPerson = 10,
                ArrivalDate = new DateTimeOffset(2019, 11, 24, 10, 0, 0, TimeSpan.Zero),
                DepartureDate = new DateTimeOffset(2019, 11, 24, 1, 0, 0, TimeSpan.Zero),
                CheckInDate = new DateTimeOffset(2019, 11, 24, 11, 0, 0, TimeSpan.Zero),
                MaxPerson = 3,
                Nights = 10,
                Provider = "sth",
                RoomType = RoomType.Standard,
                DepartureCity = departureCity,
                Hotel = hotel,
            };

            var priceExcess = tour1.PricePerPerson * _options.Value.MaxPriceExcess;

            var tour3 = new Tour
            {
                PricePerPerson = tour1.PricePerPerson - priceExcess - 0.05M,
                ArrivalDate = new DateTimeOffset(2019, 11, 24, 10, 0, 0, TimeSpan.Zero),
                DepartureDate = new DateTimeOffset(2019, 11, 24, 1, 0, 0, TimeSpan.Zero),
                CheckInDate = new DateTimeOffset(2019, 11, 24, 11, 0, 0, TimeSpan.Zero),
                MaxPerson = 3,
                Nights = 10,
                Provider = "sth",
                RoomType = RoomType.Standard,
                DepartureCity = departureCity,
                Hotel = hotel,
            };

            var tours = new[] { tour1, tour2, tour3 };

            var aggregated = _tourAggregator.Aggregate(tours.ToAsyncEnumerable());
            Assert.AreSame(await aggregated.SingleAsync(), tour3);

            tours = new[] { tour1 };

            aggregated = _tourAggregator.Aggregate(tours.ToAsyncEnumerable());
            Assert.AreSame(await aggregated.SingleAsync(), tour1);
        }
    }
}
